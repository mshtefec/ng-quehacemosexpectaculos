// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCMSBzh98SqhjZNxZwIU4ExoV4G-jRvpSo",
    authDomain: "ng-quehacemosexpectaculos.firebaseapp.com",
    databaseURL: "https://ng-quehacemosexpectaculos.firebaseio.com",
    projectId: "ng-quehacemosexpectaculos",
    storageBucket: "ng-quehacemosexpectaculos.appspot.com",
    messagingSenderId: "120911203258",
    appId: "1:120911203258:web:a8470759e758f3ed"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
